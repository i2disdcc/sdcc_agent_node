function fillbody(info) {
	$(".modify").hide();
	document.getElementById("descriptionIn").value = info.description;
	document.getElementById("keyDis").innerHTML = info.keyS;
	document.getElementById("ownerDis").innerHTML = info.owner;
	document.getElementById("descriptionDis").innerHTML = info.body.description;
	
}

function switchTo(){
	$(".modify").toggle();
	$(".display").toggle();
}

function sendRequest() {

	
	$(".overlay").show();
	
	var requestBody = {
			key : $("#keyDis").text(),
			owner : $("#ownerDis").text(),
			body : {
				description : $("#descriptionIn").val()
			}
		}


	$.ajax({

		url : './update',

		type : 'post',
		contentType : 'application/json',
		async: false,
		data : JSON.stringify(requestBody),
		success : function(data, text) {
			location.reload();
		},
		error : function(request, status, error) {
			alert(request.responseText);
		}
	});
	
	
	$(".overlay").hide();

}

function verify() {

	var key=$("#keyDis").text();
	var urls='./verify?key='+key;
	$(".overlay").show();
	


	$.ajax({

		url : urls ,

		type : 'post',
		contentType : 'application/json',
		async: false,
		
		success : function(data, text) {
			alert("Object verified result:" + JSON.stringify(data));
		},
		error : function(request, status, error) {
			alert(request.responseText);
		}
	});
	
	
	$(".overlay").hide();

}
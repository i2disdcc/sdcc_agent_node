function senKeys() {

	var initialize = {
			numKeys : $("#numKey").val(),
			start : $("#fromKey").val(),
			stop : $("#toKey").val()
	}
	$.ajax({

		url : './createKeysOnChord',

		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(initialize)
	});

}
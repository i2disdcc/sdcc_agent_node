/**
 * 
 */
function sendRequest() {
	$(".overlay").show();
	var requestBody = {
		key : $("#key").val(),
		owner : $("#owner").val(),
		body : {
			description : $("#description").val()
		}
	}

	$.ajax({

		url : ('./create'),

		type : 'post',
		contentType : 'application/json',
		async : false,
		data : JSON.stringify(requestBody),
		success : function(data, text) {
			location.href = './allLicenses';
		},
		error : function(request, status, error) {
			alert(request.responseText);
		}
	});
	
	$(".overlay").hide();
}
function fillGames(games, loadBalancerURL, details, deletegame) {
	var arrayLenght = games.length;
	var body = $("#gamespanels");
	for (var i = 0; i < arrayLenght;) {

		var row = $(document.createElement('div'));
		row.addClass('row');
		body.append(row);
		// aggiunta riga
		var j = 0;
		while (i < arrayLenght && j < 4) {
			// aggiunge un ogg
			var col = $(document.createElement('div'));
			col.addClass('col-lg-3 col-md-6 button');
			col.attr("onclick", "location.href='" + details + "?key="
					+ games[i].keyS + "';");
			row.append(col);
			var pane = $(document.createElement('div'));
			pane.addClass('panel panel-primary');
			col.append(pane);
			var panehead = $(document.createElement('div'));
			panehead.addClass('panel-heading');
			pane.append(panehead);
			var temprow = $(document.createElement('div'));
			temprow.addClass("row");
			panehead.append(temprow);
			var temp1 = $(document.createElement('div'));
			temp1.addClass("col-xs-3");
			var temp2 = $(document.createElement('div'));

			temprow.append(temp1);
			temp1 = $(document.createElement('div'));
			temp1.addClass("col-xs-12 text-right");
			temprow.append(temp1);
			temp2 = $(document.createElement('div'));
			temp2.append("Key: ");
			temp2.append(games[i].keyS);
			temp1.append(temp2);
			temp2 = $(document.createElement('div'));
			temp2.append("Owner: ");
			temp2.append(games[i].owner);
			temp1.append(temp2);
			if (!games[i].descritpion == null) {
				if (!games[i].descritpion.equals("")) {
					temp2 = $(document.createElement('div'));
					temp2.append("Description: ");
					temp2.append(games[i].body.descritpion);
					temp1.append(temp2);
				}
			}
			temp2 = $(document.createElement('span'));
			temp2
					.append("<span class='glyphicon glyphicon-remove' aria-hidden='true'></span>");
			temp2.attr("class", "btn btn-danger");
			// temp2.attr("onclick", "location.href='" + deletegame + "?id="
			// + games[i].key + "';event.stopPropagation();");
			temp2.attr("onclick", "deletegames('" + deletegame + "?key="
					+ games[i].keyS + "', event);");
			temp1.append(temp2);
			i++;
			j++;

		}
	}
}

function deletegames(url, event) {
	event.stopPropagation();
	$(".overlay").show();
	

	$.ajax({

		url : url,

		type : 'post',
		contentType : 'application/json',
		async: false,
		success : function(data, text) {
			location.reload();
		},
		error : function(request, status, error) {
			alert(request.responseText);
		}
		
	});
	$(".overlay").hide();

}

function senKeys() {
	$(".overlay").show();

	var initialize = {
		numKeys : $("#numKey").val(),
		start : $("#fromKey").val(),
		stop : $("#toKey").val(),
		ip: $("#ip").val()
	}

	$.ajax({

		url : './InitializeFirstNode',

		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(initialize),
		async: false,
		success : function(data, text) {
			location.href='./allLicenses';
		},
		error : function(request, status, error) {
			alert(request.responseText);
		}
	});

	$(".overlay").hide();

	
}
package capri.fabiani.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

public class HttpEntityConfiguration {
	private static RestTemplate template = null;
	
	@Bean
	public static RestTemplate getTemplate() {
		if(template == null)
			template = new RestTemplate(clientHttpRequestFactory());
		return template;
	}
	
	private static ClientHttpRequestFactory clientHttpRequestFactory() {
	    HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
	    factory.setReadTimeout(Constants.READ_TIMEOUT);
	    factory.setConnectTimeout(Constants.CONNECTION_TIMEOUT);
	    return factory;
	}   
}

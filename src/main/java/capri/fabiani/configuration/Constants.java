package capri.fabiani.configuration;

public class Constants {

	public final static String RESPONSE_CONTROLLER="/response";
	public static final String VERIFY_WORKER_URL = "/business";
	public final static String HTTP_PRIMER = "http://";
	public final static String BUSINESS_DELETE_PRODUCT = "/deleteProduct/";
	public final static String BUSINESS_GET_PRODUCT = "/getProduct/";
	public final static String BUSINESS_REGISTER_PRODUCT = "/registerProduct/";
	public final static String BUSINESS_UPDATE_PRODUCT = "/updateProduct/";
	public final static String WORKER_CRUD_IP = "127.0.0.1";
	public static final long RESPONSE_AWAIT = 25000; //25 sec
	public static final String ERROR_ON_SEND_REQUEST = "Error while sending message";
	public static final String RESPONSE_URL = "/response";
	public static final String SUCCESSFUL_MESSAGE = "Success!";
	public static final int NUMBER_OF_MAX_DATUM_TO_SEND = 10;
	public static final int RETRIES = 5;
	public static final String HELLO = "/getInitialINfo";
	public static final String INITIALIZE = "/InitializeFirstNode";
	public static final String RECEIVE_KEYS = "/getInitialKeys";
	
	public static final int READ_TIMEOUT = 10000;
	public static final int CONNECTION_TIMEOUT = 10000;
	public static final String[] OWNERS={"Cucciolo", "Eolo", "Brontolo", "Mammolo", "Pisolo", "Gongolo", "Dotto"};
	public static final String AUTO_GENE_DESCRIPTION = "Automatically generated description";
	public static final String CREATE_KEYS = "/createKeysOnChord";
	public static final String NAME_APP="/workernode";
//	public static final String PORT=":8080";
	public static final boolean VERBOSE = true;
	public static final long MAX_VALUE = Long.MAX_VALUE;
	public static final long MIN_VALUE = 0;
	public static final String EMPTY_DESCRIPTION = "";
	public static final String LOAD_BALANCER_URL = "WorkernodeLoadBalancing-1260350277.us-west-2.elb.amazonaws.com/workernode";
	public static final String CREATE_FORM = "datumFormCreate";
	public static final String DETAILS = "./details";
	public static final String DELETE_GAMES = "./delete";
	public static final String MODIFY_FORM = "modifyFormCreate";
	
}

package capri.fabiani.controller;

import java.util.List;
import capri.fabiani.model.rest.BusinessDTO;

public interface DatumService {
	
	public BusinessDTO create(BusinessDTO dto);
	
	public void delete(long id);
	
	public void delete(BusinessDTO dto);
	
	public void saveBulk(List<BusinessDTO> dtoList);
	
	public void update(BusinessDTO dto);
	
	public BusinessDTO findById(long id);
	
	public List<BusinessDTO> findByIdRange(long startId, long endId);
	
	public Long deleteByRange(long startId, long endId);
	
	public List<BusinessDTO> findByProductClass(long classId);
	
	public List<BusinessDTO> findByProductClassAndOwner(long productClass, String owner);

	public Long countByIdRange(long startId, long endId);

	public long findMedian(long startId, long endId);
}

package capri.fabiani.controller.implementation;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import capri.fabiani.configuration.Constants;
import capri.fabiani.configuration.HttpEntityConfiguration;
import capri.fabiani.controller.InitializzationService;
import capri.fabiani.controller.LicenseService;
import capri.fabiani.model.Identity;
import capri.fabiani.model.rest.DTO;
import capri.fabiani.model.rest.HelloDTO;
import capri.fabiani.rest.InitializeDTO;
import capri.fabiani.util.SenderReceiverUtilsAgent;


@Service("InitializzationService")
public class InitializzationServiceImplementation implements InitializzationService {
	
	@Autowired
	SenderReceiverUtilsAgent senderReceiverUtilsAgent;
	
	
	@Override
	public ResponseEntity<DTO> InitializeFirstNode(InitializeDTO initializeDTO) {	
		String ip = initializeDTO.getIp();
		ResponseEntity<DTO> response=null;
		long endId = Long.MAX_VALUE;
		long startId = 0;
		InetAddress address = null;
		try {
			address = InetAddress.getByName(ip);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Identity id = new Identity(address,endId ) ;
		HelloDTO helloDTO = new HelloDTO();
		helloDTO.setIdPred(null);
		helloDTO.setIdSucc(null);
		int numMessages = new Double((Math.ceil(((double) initializeDTO.getNumKeys()) / Constants.NUMBER_OF_MAX_DATUM_TO_SEND))).intValue();
		helloDTO.setNumMessages(numMessages);
		helloDTO.setUpperBoundInterval(endId);
		boolean successfullSent = false;
		int retries = Constants.RETRIES;
		while (!successfullSent && retries > 0) {
			retries--;
			URI uri = null;
			try {
				String sUri = "http://"+ip+ Constants.NAME_APP+"/initializzation" + Constants.HELLO;
				uri = new URI(sUri);
				
			} catch (URISyntaxException e) {

				e.printStackTrace();
			}
			RequestEntity<HelloDTO> request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(helloDTO);
			// non è gestito il caso di errore
			RestTemplate restTemplate = HttpEntityConfiguration.getTemplate();
			restTemplate.exchange(request, String.class);
			try {
				uri = new URI(Constants.HTTP_PRIMER+ip+ Constants.NAME_APP+"/initializzation" + Constants.RECEIVE_KEYS);
			} catch (URISyntaxException e) {

				e.printStackTrace();
			}
			successfullSent = senderReceiverUtilsAgent.sendInitialKeys(uri,
					startId, endId, initializeDTO.getNumKeys());
		}
		if (successfullSent) {
			DTO dto=new DTO();
			dto.setMessage(Constants.SUCCESSFUL_MESSAGE);
			response = new ResponseEntity<DTO>(dto, HttpStatus.OK);
		} else {
			DTO dto=new DTO();
			dto.setMessage(Constants.ERROR_ON_SEND_REQUEST);
			response = new ResponseEntity<DTO>(dto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@Override
	public ResponseEntity<DTO> SendKeys(InitializeDTO initializeDTO) {
		
		senderReceiverUtilsAgent.sendKeys(initializeDTO.getStart(), initializeDTO.getStop(), initializeDTO.getNumKeys(),Constants.LOAD_BALANCER_URL);
		return null;
		
	}

	
	

}

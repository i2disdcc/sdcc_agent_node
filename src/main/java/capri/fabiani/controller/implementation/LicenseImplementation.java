package capri.fabiani.controller.implementation;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.amazonaws.util.EC2MetadataUtils;
import com.google.common.base.Throwables;

import capri.fabiani.components.Responses;
import capri.fabiani.configuration.Constants;
import capri.fabiani.configuration.HttpEntityConfiguration;
import capri.fabiani.controller.DatumService;
import capri.fabiani.controller.LicenseService;
import capri.fabiani.model.License;
import capri.fabiani.model.rest.BasicDatumDTO;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.DTO;
import capri.fabiani.model.rest.DTOLicense;
import capri.fabiani.model.rest.DTOresponse;
import capri.fabiani.repositories.LicenseRepository;
import capri.fabiani.util.Logger;

@Service("LicenseService")
public class LicenseImplementation implements LicenseService {

	@Autowired
	Responses responses;
	@Autowired
	LicenseRepository licenseRepository;
	@Autowired 
	DatumService datumService;

	@Override
	public ResponseEntity<DTOresponse> createGame(DTOLicense dtoLicense) {
		License license = new License(dtoLicense.getName(), dtoLicense.getUserId(), dtoLicense.getHours(),
				dtoLicense.getHours(), dtoLicense.getGameLink());
		licenseRepository.save(license);
		DTOresponse dtoresponse = new DTOresponse(license.getId(), license.getUserId(), license.getName(),
				license.getHours(), license.getImagePath(), license.getGameLink());
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);
		return response;

	}

	@Override
	public ResponseEntity<DTOresponse> getAllGames() {
		List<License> licenses = (List<License>) licenseRepository.findAll();
		DTOresponse dtoresponse = new DTOresponse(new DTO(), null);
		dtoresponse.setGames(licenses);
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);
		return response;
	}

	public List<License> getAllGamesFromStore() {
		return (List<License>) licenseRepository.findAll();
	}

	@Override
	public ResponseEntity<BusinessDTO> verify(DTOLicense dtoLicense) {
		int requestId = responses.getRequestId();
		License license = licenseRepository.findOne(dtoLicense.getId());
		BusinessDTO requestBody = new BusinessDTO();
		requestBody.setKey(Integer.parseInt(dtoLicense.getId()));
		requestBody.setOwner(license.getUserId());
		// BasicDatumDTO bddto = new BasicDatumDTO();
		// bddto.setDescription("null desc");
		// requestBody.setBody(bddto);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate template = new RestTemplate();
		String URL = Constants.HTTP_PRIMER + Constants.WORKER_CRUD_IP + Constants.BUSINESS_GET_PRODUCT;
		// HttpEntity<BusinessDTO> entity = new
		// HttpEntity<BusinessDTO>(requestBody, headers);

		URI uri = null;
		try {
			uri = new URI(URL);
		} catch (URISyntaxException e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			BusinessDTO dtoresponse = new BusinessDTO();
			dtoresponse.setError(Constants.ERROR_ON_SEND_REQUEST);
			return new ResponseEntity<BusinessDTO>(dtoresponse, HttpStatus.I_AM_A_TEAPOT);
		}
		RequestEntity<BusinessDTO> request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON)
				.body(requestBody);
		// send request and parse result
		ResponseEntity<DTOresponse> requestResponse = template.exchange(URL, HttpMethod.POST, request,
				DTOresponse.class);

		if (requestResponse.getStatusCode() == HttpStatus.OK) {
			DTOresponse dtoresponse = new DTOresponse(new DTO(), null);
			requestResponse = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);
		} else {
			Logger.error("Errore nell'invio della verify");
			BusinessDTO dtoresponse = new BusinessDTO();
			dtoresponse.setError(Constants.ERROR_ON_SEND_REQUEST);
			return new ResponseEntity<BusinessDTO>(dtoresponse, HttpStatus.I_AM_A_TEAPOT);
		}

		ResponseEntity<BusinessDTO> response = (new ResponseEntity<BusinessDTO>(
				responses.pull(Integer.toString(requestId)), HttpStatus.OK));
		return response;
	}

	@Override
	public ResponseEntity<BusinessDTO> update(DTOLicense dtoLicense) {
		int requestId = responses.getRequestId();
		License license = licenseRepository.findOne(dtoLicense.getId());
		BusinessDTO requestBody = new BusinessDTO();
		requestBody.setKey(Integer.parseInt(dtoLicense.getId()));
		requestBody.setOwner(license.getUserId());
		BasicDatumDTO bddto = new BasicDatumDTO();
		bddto.setDescription(dtoLicense.getDescription());
		requestBody.setBody(bddto);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate template = new RestTemplate();
		String URL = Constants.LOAD_BALANCER_URL + Constants.BUSINESS_GET_PRODUCT;// TODO
																					// capire
																					// qui
																					// cosa
																					// serve
		HttpEntity<BusinessDTO> entity = new HttpEntity<BusinessDTO>(requestBody, headers);
		// send request and parse result
		ResponseEntity<DTOresponse> requestResponse = template.exchange(URL, HttpMethod.POST, entity,
				DTOresponse.class);
		if (requestResponse.getStatusCode() == HttpStatus.OK) {
			// DTOresponse dtoresponse= new DTOresponse(new DTO(), null);
			// requestResponse = new ResponseEntity<DTOresponse>(dtoresponse,
			// HttpStatus.OK);
		} else {

			BusinessDTO dtoresponse = new BusinessDTO();
			dtoresponse.setError(Constants.ERROR_ON_SEND_REQUEST);
			return new ResponseEntity<BusinessDTO>(dtoresponse, HttpStatus.I_AM_A_TEAPOT);
		}
		ResponseEntity<BusinessDTO> response = (new ResponseEntity<BusinessDTO>(
				responses.pull(Integer.toString(requestId)), HttpStatus.OK));
		return response;
	}

	@Override
	public ResponseEntity<BusinessDTO> delete(DTOLicense dtoLicense) {
		int requestId = responses.getRequestId();
		License license = licenseRepository.findOne(dtoLicense.getId());
		BusinessDTO requestBody = new BusinessDTO();
		requestBody.setKey(Integer.parseInt(dtoLicense.getId()));
		requestBody.setOwner(license.getUserId());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate template = new RestTemplate();
		String URL = Constants.VERIFY_WORKER_URL;
		HttpEntity<BusinessDTO> entity = new HttpEntity<BusinessDTO>(requestBody, headers);
		// send request and parse result
		ResponseEntity<DTOresponse> requestResponse = template.exchange(URL, HttpMethod.POST, entity,
				DTOresponse.class);
		if (requestResponse.getStatusCode() == HttpStatus.OK) {
			// DTOresponse dtoresponse= new DTOresponse(new DTO(), null);
			// requestResponse = new ResponseEntity<DTOresponse>(dtoresponse,
			// HttpStatus.OK);
		} else {

			BusinessDTO dtoresponse = new BusinessDTO();
			dtoresponse.setError(Constants.ERROR_ON_SEND_REQUEST);
			return new ResponseEntity<BusinessDTO>(dtoresponse, HttpStatus.I_AM_A_TEAPOT);
		}
		ResponseEntity<BusinessDTO> response = (new ResponseEntity<BusinessDTO>(
				responses.pull(Integer.toString(requestId)), HttpStatus.OK));
		return response;
	}

	@Override
	public ResponseEntity<BusinessDTO> create(DTOLicense dtoLicense) {
		int requestId = responses.getRequestId();
		// License license = licenseRepository.findOne(dtoLicense.getId());
		License license = new License(dtoLicense.getName(), dtoLicense.getUserId(), dtoLicense.getHours(),
				dtoLicense.getImagePath(), dtoLicense.getGameLink());

		BusinessDTO requestBody = new BusinessDTO();
		requestBody.setKey(Integer.parseInt(dtoLicense.getId()));
		requestBody.setOwner(license.getUserId());
		BasicDatumDTO bddto = new BasicDatumDTO();
		bddto.setDescription(dtoLicense.getDescription());
		requestBody.setBody(bddto);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate template = new RestTemplate();
		String URL = Constants.VERIFY_WORKER_URL;
		HttpEntity<BusinessDTO> entity = new HttpEntity<BusinessDTO>(requestBody, headers);
		// send request and parse result
		ResponseEntity<DTOresponse> requestResponse = template.exchange(URL, HttpMethod.POST, entity,
				DTOresponse.class);
		if (requestResponse.getStatusCode() == HttpStatus.OK) {
			// DTOresponse dtoresponse= new DTOresponse(new DTO(), null);
			// requestResponse = new ResponseEntity<DTOresponse>(dtoresponse,
			// HttpStatus.OK);
		} else {

			BusinessDTO dtoresponse = new BusinessDTO();
			dtoresponse.setError(Constants.ERROR_ON_SEND_REQUEST);
			return new ResponseEntity<BusinessDTO>(dtoresponse, HttpStatus.I_AM_A_TEAPOT);
		}
		ResponseEntity<BusinessDTO> response = (new ResponseEntity<BusinessDTO>(
				responses.pull(Integer.toString(requestId)), HttpStatus.OK));
		licenseRepository.save(license);
		return response;
	}

	@Override
	public ResponseEntity<BusinessDTO> verify(BusinessDTO dtoLicense) {
		try {
			String responseUrl = Constants.HTTP_PRIMER + EC2MetadataUtils.getNetworkInterfaces().get(0).getPublicHostname();
			responseUrl += "/agentnode"+ Constants.RESPONSE_URL;
			dtoLicense.setResponseURL(responseUrl);
			Logger.status("Sending verify with body: " + dtoLicense.toString());
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}
		int requestId = responses.getRequestId();
		dtoLicense.setRequestId(""+requestId);
		BusinessDTO responseBody;
		HttpStatus responseStatus;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate template = new RestTemplate();
		String url = Constants.HTTP_PRIMER + Constants.LOAD_BALANCER_URL + Constants.BUSINESS_GET_PRODUCT;
		URI uri = null;
		try {
			uri = new URI(url);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			Logger.error(Throwables.getStackTraceAsString(e));
		}
		RequestEntity<BusinessDTO> request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON)
				.body(dtoLicense);
		// TODO non è gestito il caso di errore
		RestTemplate restTemplate = HttpEntityConfiguration.getTemplate();
		ResponseEntity<String> response = restTemplate.exchange(request, String.class);

		if (response.getStatusCode() != HttpStatus.OK) {
			responseBody = new BusinessDTO();
			responseBody.setError(Constants.ERROR_ON_SEND_REQUEST);
			return new ResponseEntity<BusinessDTO>(responseBody, HttpStatus.I_AM_A_TEAPOT);
		}

		if ((responseBody = responses.pull(Integer.toString(requestId))) != null) {
			if (responseBody.getError() == null) {
				Logger.status("Request " + requestId + " successfully executed.");
				responseStatus = HttpStatus.OK;
			} else {
				Logger.error("Request " + requestId + " terminated with error " + responseBody.getError());
				responseStatus = HttpStatus.I_AM_A_TEAPOT;
			}
		} else {
			responseBody= new BusinessDTO();
			Logger.error("Request " + requestId + " timed out.");
			responseBody.setError("It seems i cannot receive response");
			responseStatus = HttpStatus.I_AM_A_TEAPOT;
		}

		ResponseEntity<BusinessDTO> result = (new ResponseEntity<BusinessDTO>(responseBody, responseStatus));
		return result;
	}

	@Override
	public ResponseEntity<BusinessDTO> update(BusinessDTO dtoLicense) {
		try {
			String responseUrl = Constants.HTTP_PRIMER + EC2MetadataUtils.getNetworkInterfaces().get(0).getPublicHostname();
			responseUrl += "/agentnode"+ Constants.RESPONSE_URL;
			dtoLicense.setResponseURL(responseUrl);
			Logger.status("Sending update with body: " + dtoLicense.toString());
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}
		int requestId = responses.getRequestId();
		dtoLicense.setRequestId(""+requestId);
		BusinessDTO responseBody;
		HttpStatus responseStatus;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate template = new RestTemplate();
		String url = Constants.HTTP_PRIMER + Constants.LOAD_BALANCER_URL + Constants.BUSINESS_UPDATE_PRODUCT;
		URI uri = null;
		try {
			uri = new URI(url);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			Logger.error(Throwables.getStackTraceAsString(e));
		}
		RequestEntity<BusinessDTO> request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON)
				.body(dtoLicense);
		// TODO non è gestito il caso di errore
		RestTemplate restTemplate = HttpEntityConfiguration.getTemplate();
		ResponseEntity<String> response = restTemplate.exchange(request, String.class);
		if (response.getStatusCode() != HttpStatus.OK) {
			responseBody = new BusinessDTO();
			responseBody.setError(Constants.ERROR_ON_SEND_REQUEST);
			return new ResponseEntity<BusinessDTO>(responseBody, HttpStatus.I_AM_A_TEAPOT);
		}

		if ((responseBody = responses.pull(Integer.toString(requestId))) != null) {
			if (responseBody.getError() == null) {
				Logger.status("Request " + requestId + " successfully executed.");
				responseStatus = HttpStatus.OK;
				datumService.update(dtoLicense);
			} else {
				Logger.error("Request " + requestId + " terminated with error " + responseBody.getError());
				responseStatus = HttpStatus.I_AM_A_TEAPOT;
			}
		} else {
			responseBody= new BusinessDTO();
			Logger.error("Request " + requestId + " timed out.");
			responseStatus = HttpStatus.I_AM_A_TEAPOT;
		}

		ResponseEntity<BusinessDTO> result = (new ResponseEntity<BusinessDTO>(responseBody, responseStatus));
		return result;
	}

	@Override
	public ResponseEntity<BusinessDTO> delete(BusinessDTO dtoLicense) {
		try {
			String responseUrl = Constants.HTTP_PRIMER + EC2MetadataUtils.getNetworkInterfaces().get(0).getPublicHostname();
			responseUrl += "/agentnode"+ Constants.RESPONSE_URL;
			dtoLicense.setResponseURL(responseUrl);
			Logger.status("Sending delete with body: " + dtoLicense.toString());
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}
		int requestId = responses.getRequestId();
		dtoLicense.setRequestId(""+requestId);
		BusinessDTO responseBody;
		HttpStatus responseStatus;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate template = new RestTemplate();
		String url = Constants.HTTP_PRIMER + Constants.LOAD_BALANCER_URL + Constants.BUSINESS_DELETE_PRODUCT;
		URI uri = null;
		try {
			uri = new URI(url);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			Logger.error(Throwables.getStackTraceAsString(e));
		}
		RequestEntity<BusinessDTO> request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON)
				.body(dtoLicense);
		// TODO non è gestito il caso di errore
		RestTemplate restTemplate = HttpEntityConfiguration.getTemplate();
		ResponseEntity<String> response = restTemplate.exchange(request, String.class);
		if (response.getStatusCode() != HttpStatus.OK) {
			responseBody = new BusinessDTO();
			responseBody.setError(Constants.ERROR_ON_SEND_REQUEST);
			return new ResponseEntity<BusinessDTO>(responseBody, HttpStatus.I_AM_A_TEAPOT);
		}

		if ((responseBody = responses.pull(Integer.toString(requestId))) != null) {
			if (responseBody.getError() == null) {
				Logger.status("Request " + requestId + " successfully executed.");
				responseStatus = HttpStatus.OK;
				datumService.delete(dtoLicense.getKey());
			} else {
				Logger.error("Request " + requestId + " terminated with error " + responseBody.getError());
				responseStatus = HttpStatus.I_AM_A_TEAPOT;
			}
		} else {
			responseBody= new BusinessDTO();
			Logger.error("Request " + requestId + " timed out.");
			responseBody.setError("It seems i cannot receive response");
			responseStatus = HttpStatus.I_AM_A_TEAPOT;
		}

		ResponseEntity<BusinessDTO> result = (new ResponseEntity<BusinessDTO>(responseBody, responseStatus));
		return result;
	}

	@Override
	public ResponseEntity<BusinessDTO> create(BusinessDTO dtoLicense) {
		try {
			String responseUrl = Constants.HTTP_PRIMER + EC2MetadataUtils.getNetworkInterfaces().get(0).getPublicHostname();
			responseUrl += "/agentnode"+ Constants.RESPONSE_URL;
			dtoLicense.setResponseURL(responseUrl);
			Logger.status("Sending create with body: " + dtoLicense.toString());
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}
		int requestId = responses.getRequestId();
		// License license = licenseRepository.findOne(dtoLicense.getId());
		dtoLicense.setRequestId(""+requestId);
		BusinessDTO responseBody;
		HttpStatus responseStatus;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate template = new RestTemplate();
		String url = Constants.HTTP_PRIMER + Constants.LOAD_BALANCER_URL + Constants.BUSINESS_REGISTER_PRODUCT;
		URI uri = null;
		try {
			uri = new URI(url);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			Logger.error(Throwables.getStackTraceAsString(e));
		}
		RequestEntity<BusinessDTO> request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON)
				.body(dtoLicense);
		// TODO non è gestito il caso di errore
		RestTemplate restTemplate = HttpEntityConfiguration.getTemplate();
		ResponseEntity<String> response = restTemplate.exchange(request, String.class);
		if (response.getStatusCode() != HttpStatus.OK) {
			responseBody = new BusinessDTO();
			responseBody.setError(Constants.ERROR_ON_SEND_REQUEST);
			return new ResponseEntity<BusinessDTO>(responseBody, HttpStatus.I_AM_A_TEAPOT);
		}

		if ((responseBody = responses.pull(Integer.toString(requestId))) != null) {
			if (responseBody.getError() == null) {
				Logger.status("Request " + requestId + " successfully executed.");
				responseStatus = HttpStatus.OK;
				datumService.create(dtoLicense);
			} else {
				Logger.error("Request " + requestId + " terminated with error " + responseBody.getError());
				responseStatus = HttpStatus.I_AM_A_TEAPOT;
			}
		} else {
			responseBody= new BusinessDTO();
			Logger.error("Request " + requestId + " timed out.");
			responseBody.setError("It seems i cannot receive response");
			responseStatus = HttpStatus.I_AM_A_TEAPOT;
		}

		ResponseEntity<BusinessDTO> result = (new ResponseEntity<BusinessDTO>(responseBody, responseStatus));
		return result;
	}

	@Override
	public ResponseEntity<DTOLicense> details(String id) {

		DTOLicense responseBody = new DTOLicense();
		License target;
		ResponseEntity<DTOLicense> responseEntity;
		HttpStatus httpStatus;
		try {

			target = licenseRepository.findOne(id);
			responseBody.setName(target.getName());
			responseBody.setGameLink(target.getGameLink());
			responseBody.setImagePath(target.getImagePath());
			responseBody.setHours(target.getHours());

			httpStatus = HttpStatus.OK;
		} catch (Exception e) {
			responseBody.setError(Throwables.getStackTraceAsString(e));
			httpStatus = HttpStatus.NOT_FOUND;
		}

		responseEntity = new ResponseEntity(responseBody, httpStatus);

		return responseEntity;
	}

	@Override
	public ResponseEntity<DTOresponse> createGame(String name, String userId, String hours, String imagePath,
			String gameLink) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<DTO> getResponse(BusinessDTO businessDTO) {
		responses.push(businessDTO);
		DTO dtoResponse = new DTO();
		dtoResponse.setMessage(Constants.SUCCESSFUL_MESSAGE);
		ResponseEntity<DTO> response = new ResponseEntity<DTO>(dtoResponse, HttpStatus.OK);
		return response;
	}

}

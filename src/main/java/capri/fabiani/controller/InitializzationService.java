package capri.fabiani.controller;

import org.springframework.http.ResponseEntity;

import capri.fabiani.model.rest.DTO;
import capri.fabiani.rest.InitializeDTO;

public interface InitializzationService {
	ResponseEntity<DTO> InitializeFirstNode(InitializeDTO initializeDTO);

	ResponseEntity<DTO> SendKeys(InitializeDTO initializeDTO);
}

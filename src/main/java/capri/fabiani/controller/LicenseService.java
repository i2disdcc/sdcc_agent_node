package capri.fabiani.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import capri.fabiani.model.License;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.DTO;
import capri.fabiani.model.rest.DTOLicense;
import capri.fabiani.model.rest.DTOresponse;


public interface LicenseService {
	ResponseEntity<DTOresponse> createGame(String name, String userId, String hours, String imagePath, String gameLink);
	ResponseEntity<DTOresponse> getAllGames();
	List<License> getAllGamesFromStore();
	ResponseEntity<BusinessDTO> verify(DTOLicense dtoGame);
	ResponseEntity<BusinessDTO> update(DTOLicense dtoGame);
	ResponseEntity<BusinessDTO> delete(DTOLicense dtoGame);
	ResponseEntity<BusinessDTO> create(DTOLicense dtoGame);
	ResponseEntity<DTOresponse> createGame(DTOLicense dtoGame);
	ResponseEntity<DTO> getResponse(BusinessDTO businessDTO);
	ResponseEntity<BusinessDTO> verify(BusinessDTO dtoLicense);
	ResponseEntity<BusinessDTO> update(BusinessDTO dtoLicense);
	ResponseEntity<BusinessDTO> delete(BusinessDTO dtoLicense);
	ResponseEntity<BusinessDTO> create(BusinessDTO dtoLicense);
	ResponseEntity<DTOLicense> details(String id);
}

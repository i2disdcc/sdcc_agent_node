package capri.fabiani.repositories;

import org.springframework.data.repository.CrudRepository;

import capri.fabiani.model.License;

public interface LicenseRepository extends CrudRepository<License, String> {
	   

}

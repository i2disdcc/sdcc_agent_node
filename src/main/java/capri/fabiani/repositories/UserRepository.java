package capri.fabiani.repositories;



import org.springframework.data.repository.CrudRepository;

import capri.fabiani.model.User;

public interface UserRepository extends CrudRepository<User, String> {
   

}

package capri.fabiani.repositories;




import org.springframework.data.repository.CrudRepository;

import capri.fabiani.model.History;

public interface HistoryRepository extends CrudRepository<History, String> {
   

}

package capri.fabiani.repositories;



import org.springframework.data.repository.CrudRepository;

import capri.fabiani.model.News;

public interface NewsRepository extends CrudRepository<News, String> {
   

}

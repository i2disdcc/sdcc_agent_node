package capri.fabiani.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import capri.fabiani.model.Datum;

@Repository
public interface DatumRepository extends MongoRepository<Datum, Long> {	
	public long countByKeyBetween(long startKey, long endKey);
	public Datum findByKey(long key);
	public List<Datum> findByKeyBetween(long startKey, long endKey);
	public List<Datum> findByOwner(String owner);
	public List<Datum> findByProductClass(long productClass);
	public List<Datum> findByProductClassAndOwner(long productClass, String owner);
	public Long deleteByKey(long key);
	public Long deleteByKeyBetween(long startKey, long endKey);
	
}

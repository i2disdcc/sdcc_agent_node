package capri.fabiani.components;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.springframework.stereotype.Component;

import capri.fabiani.configuration.Constants;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.util.Logger;

@Component
public class Responses {

	private int actualid = 0;
	private Map<String, BusinessDTO> responses;
	private Lock lock;
	private Condition newResponses;

	public Responses() {
		responses = new HashMap<String, BusinessDTO>();
		lock = new ReentrantLock();
		newResponses = lock.newCondition();
	}

	public void push(BusinessDTO businessDTO) {
		Logger.status("received response with body: " + businessDTO.toString());
		boolean isLockToUnlock = false;
		lock.lock();
		isLockToUnlock = true;
		try {
			responses.put(businessDTO.getRequestId(), businessDTO);
			newResponses.signalAll();
		} finally {
			if (isLockToUnlock)
				lock.unlock();
			isLockToUnlock = false;
		}
	}

	public BusinessDTO pull(String requestId) {
		boolean isLockToUnlock = false;
		lock.lock();
		isLockToUnlock = true;
		BusinessDTO response = null;
		long start = System.currentTimeMillis();
		try {
			while (!responses.containsKey(requestId)
					&& (System.currentTimeMillis() - start) < Constants.RESPONSE_AWAIT) {
				newResponses.await(Constants.RESPONSE_AWAIT, TimeUnit.MILLISECONDS);
			}
			if (!responses.containsKey(requestId)) {
				if (isLockToUnlock)
					lock.unlock();
				isLockToUnlock = false;
				return null;
			}
			response = responses.get(requestId);
			responses.remove(requestId, responses.get(requestId));
		} catch (InterruptedException e) {

			e.printStackTrace();
		} finally {
			if (isLockToUnlock)
				lock.unlock();
			isLockToUnlock = false;
		}
		return response;
	}

	public synchronized int getRequestId() {
		return ++actualid;
	}

}

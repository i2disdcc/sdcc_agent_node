package capri.fabiani.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import capri.fabiani.model.rest.BasicDatumDTO;



/**
 * Basic inner Datum field implementation. 
 *
 */
@Document
public class BasicDatum {
	@Field("desc")
	private String description;
	
	public BasicDatum() {}
	public BasicDatum(String description){
		this.description = description;
	}
	
	public String getDescription(){return this.description;}
	public void setDescription(String description) {this.description = description;}
	
	@Override
	public String toString() {
		return description;
	}
	
	public BasicDatumDTO pack() {
		BasicDatumDTO dto = new BasicDatumDTO();
		dto.setDescription(description);
		return dto;
	}
}

package capri.fabiani.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class License {

	@Id
	private String id;
	
	private String userId;
	private String name;
	private String hours;
	private String imagePath;
	private String gameLink;
	
	public License(String name, String usreId, String hours, String imagePath, String gameLink) {
		super();
		this.name = name;
		this.hours = hours;
		this.imagePath = imagePath;
		this.gameLink = gameLink;
		this.userId =userId;
	}
	
	
	
	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}

	

	public String getUserId() {
		return userId;
	}



	public void setUserId(String userId) {
		this.userId = userId;
	}



	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getHours() {
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getGameLink() {
		return gameLink;
	}
	public void setGameLink(String gameLink) {
		this.gameLink = gameLink;
	}
	
	
}

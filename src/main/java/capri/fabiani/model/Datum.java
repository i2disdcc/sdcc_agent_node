package capri.fabiani.model;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import capri.fabiani.configuration.Constants;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Document
public class Datum {
	@Id
	private long key;
	
	@Field("owner")
	private String owner;
	
	@Temporal(TemporalType.DATE)
	@LastModifiedDate
	private Date lastModified;
	
	@Temporal(TemporalType.DATE)
	@CreatedDate
	private Date createdDate;
	
	@Field("product_class")
	private long productClass;
	
	@Field("body")
	private BasicDatum body;
	
	public Datum() {};
	public Datum(long key, String owner) {
		this.key = key;
		this.owner = owner;
		
	}
	
	public long getKey() {return key;}
	public void setKey(long key) {this.key=key;}
	
	public String getOwner() {return owner;}
	public void setOwner(String owner) {this.owner = owner;}
	
	public Date getLastModified() {return lastModified;}
	public void setLastModified(Date lastModified) {this.lastModified=lastModified;}
	
	public Date getCreatedDate() {return createdDate;}
	public void setCreatedDate(Date createdDate) {this.createdDate=createdDate;}
	
	public long getProductClass() {return productClass;}
	public void setProductClass(long productClass) {this.productClass=productClass;}
	
	public BasicDatum getBody() {return this.body;}
	public void setBody(BasicDatum body) {this.body = body;}
	
	@Override
	public String toString() {
		return "key="+key+", owner="+owner+", creationDate="+createdDate+", lastModified="+lastModified + ", description="+
				(body == null ? Constants.EMPTY_DESCRIPTION : body.toString());
	}
	
}

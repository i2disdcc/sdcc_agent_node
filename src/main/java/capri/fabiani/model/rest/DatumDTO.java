package capri.fabiani.model.rest;
import java.util.Date;

import org.springframework.data.annotation.Id;

public class DatumDTO {
	
	private long key;
	private String owner;
	private String requestId;
	private Date createdDate;
	private Date lastModified;
	private long productClass;
	private String responseURL;
	private BasicDatumDTO body;
	
	
	public DatumDTO(){}
	
	public long getKey(){return key;};
	public void setKey(long key){this.key = key;};
	
	public String getOwner(){return owner;};
	public void setOwner(String owner){this.owner = owner;};
	
	public Date getLastModified() {return lastModified;}
	public void setLastModified(Date lastModified) {this.lastModified=lastModified;}
	
	public Date getCreatedDate() {return createdDate;}
	public void setCreatedDate(Date createdDate) {this.createdDate=createdDate;}
	
	public long getProductClass() {return productClass;}
	public void setProductClass(long productClass) {this.productClass=productClass;}

	public String getRequestId() {return requestId;}
	public void setRequestId(String requestId) {this.requestId = requestId;}

	public String getResponseURL() {return responseURL;}
	public void setResponseURL(String responseURL) {this.responseURL = responseURL;}

	public BasicDatumDTO getBody() {return body;}
	public void setBody(BasicDatumDTO body) {this.body = body;}
	
	
	
}

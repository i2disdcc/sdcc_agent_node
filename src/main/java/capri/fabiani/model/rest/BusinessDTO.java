package capri.fabiani.model.rest;


public class BusinessDTO extends DTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long key;
	private String keyS;
	private String owner;
	private long productClass;
	private String requestId;
	private String responseURL;
	private BasicDatumDTO body;
	
	public BusinessDTO(){}
	
	public long getKey(){return key;};

	public void setKey(long key) {
		this.key = key;
		this.keyS = Long.toString(key);
	};
	
	public String getOwner(){return owner;};
	public void setOwner(String owner){this.owner = owner;};
	
	public long getProductClass() {return productClass;}
	public void setProductClass(long productClass) {this.productClass=productClass;}
	
	public String getRequestId() {return requestId;}
	public void setRequestId(String requestId) {this.requestId = requestId;}

	public String getResponseURL() {return responseURL;}
	public void setResponseURL(String responseURL) {this.responseURL = responseURL;}

	public BasicDatumDTO getBody() {return body;}
	public void setBody(BasicDatumDTO body) {this.body = body;}

	public String getKeyS() {return keyS;}
	
	
	
}
package capri.fabiani.model.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import capri.fabiani.model.Identity;

public class HelloDTO extends DTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(Include.NON_NULL)
	int messageIndex;
	
	@JsonInclude(Include.NON_NULL)
	int numMessages;
	
	@JsonInclude(Include.NON_NULL)
	long upperBoundInterval=-1;
	
	@JsonInclude(Include.NON_NULL)
	Identity idPred;
	
	@JsonInclude(Include.NON_NULL)
	Identity idSucc;
	
	public HelloDTO(){}

	public int getMessageIndex() {
		return messageIndex;
	}

	public void setMessageIndex(int messageIndex) {
		this.messageIndex = messageIndex;
	}

	public int getNumMessages() {
		return numMessages;
	}

	public void setNumMessages(int numMessages) {
		this.numMessages = numMessages;
	}

	public long getUpperBoundInterval() {
		return upperBoundInterval;
	}

	public void setUpperBoundInterval(long upperBoundInterval) {
		this.upperBoundInterval = upperBoundInterval;
	}

	public Identity getIdPred() {
		return idPred;
	}

	public void setIdPred(Identity idPred) {
		this.idPred = idPred;
	}

	public Identity getIdSucc() {
		return idSucc;
	}

	public void setIdSucc(Identity idSucc) {
		this.idSucc = idSucc;
	}
	
}

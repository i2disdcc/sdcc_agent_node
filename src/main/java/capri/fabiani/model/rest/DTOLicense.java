package capri.fabiani.model.rest;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import capri.fabiani.model.License;

public class DTOLicense extends DTO {

	/**
	 * dto : - text title user
	 */
	private static final long serialVersionUID = 1L;
	@JsonInclude(Include.NON_NULL)
	private String id;
	@JsonInclude(Include.NON_NULL)
	private List<License> licenses;
	@JsonInclude(Include.NON_NULL)
	private String name;
	@JsonInclude(Include.NON_NULL)
	private String hours;
	@JsonInclude(Include.NON_NULL)
	private String imagePath;
	@JsonInclude(Include.NON_NULL)
	private String gameLink;
	@JsonInclude(Include.NON_NULL)
	private String userId;
	@JsonInclude(Include.NON_NULL)
	private String description;
	
	
	public DTOLicense() {
		super();
	}

	public DTOLicense(String name, String hours, String imagePath, String gameLink) {
		super();
		this.name = name;
		this.hours = hours;
		this.imagePath = imagePath;
		this.gameLink = gameLink;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public List<License> getGames() {
		return licenses;
	}

	public void setGames(List<License> licenses) {
		this.licenses = licenses;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getHours() {
		return hours;
	}
	
	public void setHours(String hours) {
		this.hours = hours;
	}
	
	public String getImagePath() {
		return imagePath;
	}
	
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	public String getGameLink() {
		return gameLink;
	}
	
	public void setGameLink(String gameLink) {
		this.gameLink = gameLink;
	}

	public List<License> getLicenses() {
		return licenses;
	}

	public void setLicenses(List<License> licenses) {
		this.licenses = licenses;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

	
}

package capri.fabiani.model.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import capri.fabiani.model.User;

public class DTOUser extends DTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonInclude(Include.NON_NULL)
	private User user;
	@JsonInclude(Include.NON_NULL)
	private String name;
	
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public DTOUser(){}
	
	public DTOUser(DTO dto){
		
		this.error = dto.error;
		
	}
	
	
	public DTOUser(User user, String message){
		
		this.user=user;
		this.message = message;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

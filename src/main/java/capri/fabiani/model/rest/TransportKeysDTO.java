package capri.fabiani.model.rest;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class TransportKeysDTO extends DTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(Include.NON_NULL)
	List<BusinessDTO> datas;
	@JsonInclude(Include.NON_NULL)
	
	long idSender;
	
	int messageIndex;
	
	
	
	
	public TransportKeysDTO(List<BusinessDTO> datas, long idSender, int messageIndex) {
		super();
		this.datas = datas;
		this.idSender = idSender;
		this.messageIndex = messageIndex;
	}
	public TransportKeysDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TransportKeysDTO(String error) {
		super(error);
		// TODO Auto-generated constructor stub
	}
	public List<BusinessDTO> getDatas() {
		return datas;
	}
	public void setDatas(List<BusinessDTO> datas) {
		this.datas = datas;
	}
	public long getIdSender() {
		return idSender;
	}
	public void setIdSender(long idSender) {
		this.idSender = idSender;
	}
	public int getMessageIndex() {
		return messageIndex;
	}
	public void setMessageIndex(int messageIndex) {
		this.messageIndex = messageIndex;
	}
	
	
	

}

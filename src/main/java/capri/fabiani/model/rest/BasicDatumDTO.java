package capri.fabiani.model.rest;



import capri.fabiani.model.BasicDatum;

public class BasicDatumDTO extends DTO {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 7597126954646138815L;
	
	
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public BasicDatum unpack() {
		BasicDatum bDatum = new BasicDatum();
		bDatum.setDescription(description);
		return bDatum;
	}
	
//	private long key;
//	private String owner;
//	private long productClass;
//	private Date createdDate;
//	private Date lastModified;
//	
//	public long getKey() {
//		return key;
//	}
//	public void setKey(long key) {
//		this.key = key;
//	}
//	public String getOwner() {
//		return owner;
//	}
//	public void setOwner(String owner) {
//		this.owner = owner;
//	}
//	public Date getCreatedDate() {
//		return createdDate;
//	}
//	public void setCreatedDate(Date createdDate) {
//		this.createdDate = createdDate;
//	}
//	public Date getLastModified() {
//		return lastModified;
//	}
//	public void setLastModified(Date lastModified) {
//		this.lastModified = lastModified;
//	}
//	public long getProductClass() {
//		return productClass;
//	}
//	public void setProductClass(long productClass) {
//		this.productClass = productClass;
//	}
}

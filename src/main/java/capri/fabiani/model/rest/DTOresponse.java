package capri.fabiani.model.rest;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import capri.fabiani.model.License;

public class DTOresponse extends DTO {

	public DTOresponse(DTO dto, String string) {
			
		this.error = dto.getError();
		this.message = string;
		
	}

	public DTOresponse(String gameid, String userId, String name, String hours, String imagePath, String gameLink) {
		super();
		this.gameId = gameid;
		this.name = name;
		this.hours = hours;
		this.imagePath = imagePath;
		this.gameLink = gameLink;
		this.userId = userId;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonInclude(Include.NON_NULL)
	List<License> licenses;
	
	@JsonInclude(Include.NON_NULL)
	String gameId;
	
	@JsonInclude(Include.NON_NULL)
	String userId;
	
	@JsonInclude(Include.NON_NULL)
	String name;
	
	@JsonInclude(Include.NON_NULL)
	String hours;
	
	@JsonInclude(Include.NON_NULL)
	String imagePath;
	
	@JsonInclude(Include.NON_NULL)
	String gameLink;
	
	public List<License> getGames() {
		return licenses;
	}

	public void setGames(List<License> licenses) {
		this.licenses = licenses;
	}

	public String getGameId() {
		return gameId;
	}
	
	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getHours() {
		return hours;
	}
	
	public void setHours(String hours) {
		this.hours = hours;
	}
	
	public String getImagePath() {
		return imagePath;
	}
	
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	public String getGameLink() {
		return gameLink;
	}
	
	public void setGameLink(String gameLink) {
		this.gameLink = gameLink;
	}
	
	
	
}

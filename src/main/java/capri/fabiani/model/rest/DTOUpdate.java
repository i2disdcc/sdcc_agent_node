package capri.fabiani.model.rest;

public class DTOUpdate extends DTO {

	/**
	 * dto : - newtext user
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	private String newtitle;
	private String newtext;
	private String user;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNewtitle() {
		return newtitle;
	}
	public void setNewtitle(String newtitle) {
		this.newtitle = newtitle;
	}
	public String getNewtext() {
		return newtext;
	}
	public void setNewtext(String newtext) {
		this.newtext = newtext;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
}

package capri.fabiani.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class User  {


	@Id
    private String id;

	@Indexed(unique = true)
	private String username;
	
    public User() {}
    
    public String getUsername(){
    	return this.username;   	
    }
    

    public void setUsername(String username){
    	this.username = username;
    }
   

	public String getId() {
		return this.id;
	}

	@Override
	public boolean equals(Object obj) {
		
		User temp = (User) obj;
		
		if(temp.id.equals(this.id))
			return true;
		
		return false;
		
	}
    
   
}
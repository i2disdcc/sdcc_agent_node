package capri.fabiani.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class History {

	@Id
	private String id;
	private Date date;
	

	private User editor;
	private String title;
	private String text;
	
	public History(){};
	
	public History( Date date, User editor, String title, String text){
		
		this.date = date;
		this.editor = editor;
		this.title = title;
		this.text = text;
		
		
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public User getEditor() {
		return editor;
	}
	public void setEditor(User editor) {
		this.editor = editor;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
}

package capri.fabiani.model;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.boot.bind.InetAddressEditor;

public class Identity implements Comparable<Identity> {
	protected InetAddress address;
	protected long key;
	
	public Identity() {	
		this.key=-1;
		try {
			this.address=InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			this.address=InetAddress.getLoopbackAddress();
			e.printStackTrace();
		}
	}
	
	public Identity(InetAddress address, long key) {
		this.setAddress(address);
		this.setKey(key);
	}
	
	public Identity(long key) {
		this.setKey(key);
	}

	public InetAddress getAddress() {
		return address;
	}

	public void setAddress(InetAddress address) {
		this.address = address;
	}

	public long getKey() {
		return key;
	}

	public void setKey(long key) {
		this.key = key;
	}

	@Override
	public int compareTo(Identity arg0) {
		
		if(this.key < arg0.getKey())
			return -1;
		
		if(this.key > arg0.getKey())
			return 1;
		
		return 0;
	}
	
	@Override 
	public boolean equals(Object arg0) {
		if (arg0 == null) 
			return false;
		if (arg0 == this) 
			return true;
		if (!(arg0 instanceof Identity))return false;
		
		return this.key == ((Identity)arg0).getKey();
	}
	
}
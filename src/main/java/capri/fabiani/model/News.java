package capri.fabiani.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class News {
	
	@Id
	private String id;
	private Date date;
	
	@DBRef(db = "User")
	private User creator;
	private String title;
	private String text;
	
	@DBRef(db = "User")
	private User lastEditor;
	private Date lastUpdateDate;
	
	
	public News(){};
	
	public News(Date date,User creator, String title, String text, User lastEditor, Date lastUpdateDate){
		
		this.date=date;
		this.creator = creator;
		this.title=title;
		this.text= text;
		this.lastEditor = lastEditor;
		this.lastUpdateDate = lastUpdateDate;
		
	}
	
	@DBRef(db = "History")
	private List<History> histories;

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public User getLastEditor() {
		return lastEditor;
	}
	public void setEditor(User lastEditor) {
		this.lastEditor = lastEditor;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public List<History> getHistories() {
		return histories;
	}
	public void setHistories(List<History> histories) {
		this.histories = histories;
	}
	public User getCreator() {
		return creator;
	}
	public void setCreator(User creator) {
		this.creator = creator;
	}
	
	public List<History> addHistory(History history){
		
		if(this.histories == null){
			this.histories = new ArrayList<History>();
		}
		
		this.histories.add(history);
		
		return this.histories;
		
	}
	
}

package capri.fabiani.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import capri.fabiani.model.rest.DTO;

public class InitializeDTO extends DTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(Include.NON_NULL)
	private String ip;
	@JsonInclude(Include.NON_NULL)
	private long numKeys;
	@JsonInclude(Include.NON_NULL)
	private long start;
	@JsonInclude(Include.NON_NULL)
	private long stop;
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public long getNumKeys() {
		return numKeys;
	}
	public void setNumKeys(long numKeys) {
		this.numKeys = numKeys;
	}
	public long getStart() {
		return start;
	}
	public void setStart(long start) {
		this.start = start;
	}
	public long getStop() {
		return stop;
	}
	public void setStop(long stop) {
		this.stop = stop;
	}
	
	
	
	
	
	
}

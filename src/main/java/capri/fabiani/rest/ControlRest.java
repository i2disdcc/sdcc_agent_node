package capri.fabiani.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import capri.fabiani.configuration.Constants;
import capri.fabiani.controller.DatumService;
import capri.fabiani.controller.InitializzationService;
import capri.fabiani.controller.LicenseService;
import capri.fabiani.model.License;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.DTO;
import capri.fabiani.model.rest.DTOLicense;
import capri.fabiani.model.rest.DTOresponse;

@RestController
@RequestMapping("/")
public class ControlRest {

	@Autowired
	LicenseService licenseService;
	@Autowired
	InitializzationService initializzationService;
	@Autowired
	DatumService datumService;

	@RequestMapping("/greeting")
	public Model greeting(@RequestParam(value = "name", required = false, defaultValue = "World") String name,
			Model model) {
		model.addAttribute("name", name);
		return model;
	}

	@RequestMapping("/body")
	public Model index(@RequestParam(value = "title", required = false, defaultValue = "Cloud Games") String title,
			Model model) {
		model.addAttribute("title", title);
		model.addAttribute("page", "<p>Hello World</p>");
		return model;
	}

	@RequestMapping("/hello")
	public Model hello(Model model) {
		model.addAttribute("title", "Cloud Games");
		return model;
	}
	
	@RequestMapping("/initialize")
	public Model initialize(Model model) {
		model.addAttribute("title", "Console");
		return model;
	}
	
	@RequestMapping("/sendKeys")
	public Model sendKeys(Model model) {
		model.addAttribute("title", "Console");
		return model;
	}

	@RequestMapping("/allgames")
	public Model allgames(Model model) {
		ArrayList<License> licenses = (ArrayList<License>) licenseService.getAllGamesFromStore();
		model.addAttribute("title", "Cloud Games");
		model.addAttribute("prova", "ciccio");
		model.addAttribute("licenses", licenses);
		return model;
	}
	
	@RequestMapping("/allLicenses")
	public Model allLicenses(Model model) {
		List<BusinessDTO> licenses = datumService.findByIdRange(Constants.MIN_VALUE, Constants.MAX_VALUE);
		model.addAttribute("title", "Cloud Games");
		model.addAttribute("intestation", "Your Games");
		model.addAttribute("loadBalancer", Constants.LOAD_BALANCER_URL);
		model.addAttribute("details", Constants.DETAILS);
		model.addAttribute("deletegames", Constants.DELETE_GAMES);
		model.addAttribute("licenses", licenses);
		return model;
	}

	@RequestMapping(value = "/createGame", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> createGame(@RequestBody DTOLicense dtoGame) {
		return licenseService.createGame(dtoGame.getName(), dtoGame.getUserId(), dtoGame.getHours(), dtoGame.getImagePath(),
				dtoGame.getGameLink());
	}

	@RequestMapping(value = "/getAllGames", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getAllGames() {
		return licenseService.getAllGames();
	}

	@RequestMapping(value = "/verify", method = RequestMethod.POST)
	public ResponseEntity<BusinessDTO> verify(@RequestParam(value = "key") long key) {
		BusinessDTO dtoGame = datumService.findById(key);
		return licenseService.verify(dtoGame);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseEntity<BusinessDTO> update(@RequestBody /*DTOLicense*/ BusinessDTO dtoGame) {
		return licenseService.update(dtoGame);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ResponseEntity<BusinessDTO> delete( @RequestParam(value = "key") long key) {
		BusinessDTO dtoGame = datumService.findById(key);
		return licenseService.delete(dtoGame);
	}
	
//	@RequestMapping(value = "/create", method = RequestMethod.POST)
//	public ResponseEntity<DTOresponse> create(@RequestBody DTOLicense dtoGame) {
//		return licenseService.createGame(dtoGame);
//	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BusinessDTO> create(@RequestBody BusinessDTO dtoGame) {
		return licenseService.create(dtoGame);
	}
	
	@RequestMapping(value = Constants.RESPONSE_URL, method = RequestMethod.POST)
	public ResponseEntity<DTO> getResponse(@RequestBody BusinessDTO businessDTO) {
		return licenseService.getResponse(businessDTO);
	}
	
	@RequestMapping(value = Constants.INITIALIZE, method = RequestMethod.POST)
	public ResponseEntity<DTO> initialize(@RequestBody InitializeDTO initializeDTO) {
		return initializzationService.InitializeFirstNode(initializeDTO);
	}
	
	@RequestMapping(value = Constants.CREATE_KEYS, method = RequestMethod.POST)
	public ResponseEntity<DTO> sendKeys(@RequestBody InitializeDTO initializeDTO) {
		return initializzationService.SendKeys(initializeDTO);
	}
	
	@RequestMapping(Constants.CREATE_FORM)
	public Model createForm(Model m) {
		m.addAttribute("isCreating", true);
		return m;
	}
	
		
	@RequestMapping("/details")
	public Model updateForm(Model m, @RequestParam(value = "key") long key) {
		BusinessDTO toDisplay = datumService.findById(key);
		m.addAttribute("info", toDisplay);
		return m;
	}
	

}

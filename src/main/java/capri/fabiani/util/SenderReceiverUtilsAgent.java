package capri.fabiani.util;

import java.net.InetAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import capri.fabiani.configuration.Constants;
import capri.fabiani.configuration.HttpEntityConfiguration;
import capri.fabiani.controller.DatumService;
import capri.fabiani.controller.LicenseService;
import capri.fabiani.model.rest.BasicDatumDTO;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.TransportKeysDTO;
import scala.collection.immutable.Stream.Cons;

@Component
public class SenderReceiverUtilsAgent {

	Set<Long> added;

	@Autowired
	LicenseService licenseService;

	@Autowired
	DatumService datumService;

	private SenderReceiverUtilsAgent() {
		added = new HashSet<Long>();
	}

	public boolean sendInitialKeys(URI uri, long startId, long endId, long numKeys) {
		datumService.deleteByRange(Constants.MIN_VALUE, Constants.MAX_VALUE);
		ArrayList<BusinessDTO> basicDatums = new ArrayList<>();
		int index = 0;
		if (startId < endId) {
			for (int j = 0; j < numKeys;) {
				TransportKeysDTO tkDTO = new TransportKeysDTO();
				for (int i = 0; i < Constants.NUMBER_OF_MAX_DATUM_TO_SEND && j < numKeys; i++) {

					long random = (long) ((endId - startId) * Math.random() + startId);
					while (added.contains(
							random) /* TODO oppure ho provato x volte */) {
						random = (long) ((endId - startId) * Math.random() + startId);
					}
					added.add(random);
					BusinessDTO tempDatum = new BusinessDTO();
					tempDatum.setKey(random);
					String owner = Constants.OWNERS[(int) (Math.random() * Constants.OWNERS.length)];
					tempDatum.setOwner(owner);
					BasicDatumDTO body = new BasicDatumDTO();
					body.setDescription(Constants.AUTO_GENE_DESCRIPTION);
					tempDatum.setBody(body);
					datumService.create(tempDatum);
					basicDatums.add(tempDatum);
					j++;
				}

				tkDTO.setDatas(basicDatums);
				tkDTO.setMessageIndex(index);
				RequestEntity<TransportKeysDTO> request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON)
						.body(tkDTO);
				// TODO non è gestito il caso di errore
				RestTemplate restTemplate = HttpEntityConfiguration.getTemplate();
				ResponseEntity<String> response = restTemplate.exchange(request, String.class);
				index++;
				if (response.getStatusCode() != HttpStatus.OK)
					return false;
			}
		}
		return true;
	}

	public long sendKeys(long startId, long endId, long numKeys, String ip) {
		// TODO Auto-generated method stub
		long created = 0;
		for (int i = 0; i < numKeys; i++) {

			long random = (long) ((endId - startId) * Math.random() + startId);
			while (!added.contains(random) || datumService.findById(
					random) == null /* TODO oppure ho provato x volte */) {
				random = (long) ((endId - startId) * Math.random() + startId);

			}
			added.add(random);
			BusinessDTO tempDatum = new BusinessDTO();
			tempDatum.setKey(random);
			String owner = Constants.OWNERS[(int) (Math.random() * Constants.OWNERS.length)];
			tempDatum.setOwner(owner);
			BasicDatumDTO body = new BasicDatumDTO();
			body.setDescription(Constants.AUTO_GENE_DESCRIPTION);
			tempDatum.setBody(body);
			licenseService.create(tempDatum);
			
			created++;
		}
		return created;
	}

}

package capri.fabiani.util;

import java.time.Instant;
import java.time.ZoneId;

import capri.fabiani.configuration.Constants;

public class Logger {

	public static void warning(String s) {
		if (Constants.VERBOSE)
			System.out.println(
					"[WARNING @" + getFileName() + "(" + getLineNumber() + ") " + getTimestamp() + "]: " + s);
	}

	public static void error(String s) {
		if (Constants.VERBOSE)
			System.out.println(
					"[ERROR @" + getFileName() + "(" + getLineNumber() + ") " + getTimestamp() + "]: " + s);
	}

	public static void status(String s) {
		if (Constants.VERBOSE)
			System.out.println(
					"[STATUS @" + getFileName() + "(" + getLineNumber() + ") " + getTimestamp() + "]: " + s);
	}

	public static int getLineNumber() {
		return Thread.currentThread().getStackTrace()[3].getLineNumber();
	}

	public static String getFileName() {
		return Thread.currentThread().getStackTrace()[3].getFileName();
	}

	public static String getTimestamp() {
		return Instant.now().toString();
	}

}

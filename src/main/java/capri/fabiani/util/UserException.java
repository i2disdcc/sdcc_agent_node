package capri.fabiani.util;

import org.springframework.http.ResponseEntity;

import capri.fabiani.model.rest.DTOUser;

public class UserException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ResponseEntity<DTOUser> re;
	private String message;
	
	public UserException(ResponseEntity<DTOUser> re,String message){
		
		this.setRe(re);
		this.setMessage(message);
		
	}

	public ResponseEntity<DTOUser> getRe() {
		return re;
	}

	public void setRe(ResponseEntity<DTOUser> re) {
		this.re = re;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
